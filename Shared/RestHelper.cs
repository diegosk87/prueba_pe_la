﻿using ExamenPEv1.Modelo;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Swashbuckle.Swagger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ExamenPEv1.Shared
{
    public class RestHelper
    {
        private string baseURL = "https://api.zeryo.com.mx/api/";

        //GET
        public async Task<string> GetAll()
        {
            using (HttpClient cliente = new HttpClient())
            {
                using (HttpResponseMessage respuesta = await cliente.GetAsync(baseURL + "user"))
                {
                    using (HttpContent contenido = respuesta.Content)
                    {
                        string data = await contenido.ReadAsStringAsync();
                        if (data != null)
                        {
                            return data;
                        }
                    }

                }
            }
            return string.Empty;
        }

        //POST
        public async Task<string> RegistroUsuario(string username, string mail, string password)
        {
            var inputData = new Dictionary<string, string>
            {
                { "userName", username},
                {"email", mail},
                {"password", password}
            };

            var input = new FormUrlEncodedContent(inputData);

            using (HttpClient cliente = new HttpClient())
            {
                using (HttpResponseMessage respuesta = await cliente.PostAsync(baseURL + "user", input))
                {
                    using (HttpContent contenido = respuesta.Content)
                    {
                        string data = await contenido.ReadAsStringAsync();
                        if (data != null)
                        {
                            string json = BeautifyJson(data);
                            return json;
                        }
                    }

                }
            }
            return string.Empty;
        }

        //POST
        public async Task<string> Login(string username, string password)
        {
            var inputData = new Dictionary<string, string>
            {
                { "user", username},
                {"password", password}
            };

            var input = new FormUrlEncodedContent(inputData);

            using (HttpClient cliente = new HttpClient())
            {
                using (HttpResponseMessage respuesta = await cliente.PostAsync(baseURL + "auth", input))
                {
                    using (HttpContent contenido = respuesta.Content)
                    {
                        string data = await contenido.ReadAsStringAsync();
                        if (data != null)
                        {
                            string json = BeautifyJson(data);
                            return json;
                        }
                    }

                }
            }
            return string.Empty;
        }

        //POST
        public async Task<string> SaverDatos(string idUser, string strDatos, string strToken)
        {
            var inputData = new Dictionary<string, string>
            {
                {"idUser", idUser},
                {"data", strDatos}
            };

            var input = new FormUrlEncodedContent(inputData);

            using (HttpClient cliente = new HttpClient())
            {
                cliente.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", strToken);
                using (HttpResponseMessage respuesta = await cliente.PostAsync(baseURL + "saver", input))
                {
                    using (HttpContent contenido = respuesta.Content)
                    {
                        string data = await contenido.ReadAsStringAsync();
                        if (data != null)
                        {
                            string json = BeautifyJson(data);
                            return json;
                        }
                    }

                }
            }
            return string.Empty;
        }

        //GET
        public async Task<string> GetSaverDatos(string idUser, string strToken)
        {
            var inputData = new Dictionary<string, string>
            {
                {"idUser", idUser}
            };

            var input = new FormUrlEncodedContent(inputData);

            using (HttpClient cliente = new HttpClient())
            {
                cliente.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", strToken);
                using (HttpResponseMessage respuesta = await cliente.GetAsync(baseURL + "saver/"+idUser))
                {
                    using (HttpContent contenido = respuesta.Content)
                    {
                        string data = await contenido.ReadAsStringAsync();
                        if (data != null)
                        {
                            string json = BeautifyJson(data);
                            return json;
                        }
                    }

                }
            }
            return string.Empty;
        }

        public string BeautifyJson(string strJson)
        {
            JToken parseJson = JToken.Parse(strJson);
            return parseJson.ToString(Formatting.Indented);
        }

        public string MensajeResponse(string strJson)
        {

            MsjApi msj = JsonConvert.DeserializeObject<MsjApi>(strJson);

            string mensaje = msj.message;

            return mensaje;
        }

        public Usuario GetUsuario(string strJson)
        {
            MsjApi resp = JsonConvert.DeserializeObject<MsjApi>(strJson);
            var data = resp.data;
            string json = BeautifyJson(data.ToString());
            Usuario user = JsonConvert.DeserializeObject<Usuario>(json);
            return user;
        }

        public List<Texto> GetTextos(string strJson)
        {
            MsjApi resp = JsonConvert.DeserializeObject<MsjApi>(strJson);
            var data = resp.data;
            string json = BeautifyJson(data.ToString());
            List<Texto> lst = JsonConvert.DeserializeObject<List<Texto>>(json);
            return lst;
        }
    }
    public class MsjApi
    {
        public int status { get; set; }
        public string message { get; set; }
        public object data { get; set; }
    }
    public class Texto
    {
        public int idSaver { get; set; }
        public string data { get; set; }
        public DateTime lastUpdate { get; set; }
    }
}
