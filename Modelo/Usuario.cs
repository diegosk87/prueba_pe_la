﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenPEv1.Modelo
{
    public class Usuario
    {
        public int idUser { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public string userPhoto { get; set; }
        public string token { get; set; }
    }
}
