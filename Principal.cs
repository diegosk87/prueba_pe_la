﻿using ExamenPEv1.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExamenPEv1
{
    public partial class Principal : Form
    {
        RestHelper rh = new RestHelper();
        public Principal()
        {
            InitializeComponent();
        }

        private void Principal_Load(object sender, EventArgs e)
        {
            ConsultarTexto();
            txtComentario.Focus();
        }

        private async void btnSubir_Click(object sender, EventArgs e)
        {
            if (txtComentario.Text=="")
            {
                MessageBox.Show("El campo esta vacío", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                var json = await rh.SaverDatos(lblIdUser.Text, txtComentario.Text, lblToken.Text);
                string mensaje = rh.MensajeResponse(json);
                if (mensaje == "Data saved succesfull")
                {
                    lblMensaje.Text = mensaje;
                    lblMensaje.Visible = true;
                    MessageBox.Show(mensaje + " ", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ConsultarTexto();
                    txtComentario.Text = "";
                }
                else
                    MessageBox.Show(mensaje + " ", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            ConsultarTexto();
        }

        private async void ConsultarTexto()
        {
            var jsonData = await rh.GetSaverDatos(lblIdUser.Text, lblToken.Text);
            List<Texto> lst = rh.GetTextos(jsonData.ToString());
            dgvTextos.DataSource = lst;
        }

        private void btnEndSession_Click(object sender, EventArgs e)
        {
            Login window = new Login();
            lblEmail.Text = "";
            lblIdUser.Text = "";
            lblUser.Text = "";
            lblUserPhoto.Text = "";
            lblMensaje.Text = "";
            txtComentario.Text = "";
            window.Show();
            this.Dispose();
        }

        private void txtComentario_TextChanged(object sender, EventArgs e)
        {
            lblMensaje.Text = "";
        }
        private void CerrarVentana(object sender, FormClosingEventArgs e)
        {

            DialogResult dialogo = MessageBox.Show("¿Desea cerrar el programa?",
               "Cerrar el programa", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogo == DialogResult.No)
            {
                e.Cancel = true;
            }
            else
            {
                e.Cancel = false;

            }
        }

        private void CerrarPrograma(object sender, FormClosedEventArgs e)
        {
            Application.ExitThread();

        }
    }
}
