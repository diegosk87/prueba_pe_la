﻿using ExamenPEv1.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExamenPEv1
{
    public partial class RegistroUsuario : Form
    {
        RestHelper rh = new RestHelper();
        public RegistroUsuario()
        {
            InitializeComponent();
        }

        private async void btnRegistro_Click(object sender, EventArgs e)
        {
            if (txtMail.Text == "" || txtUsername.Text == "" || txtPassword.Text == "")
            {
                MessageBox.Show("Completa los campos vacíos", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                var respuesta = await rh.RegistroUsuario(txtUsername.Text, txtMail.Text, txtPassword.Text);
                string mensaje = rh.MensajeResponse(respuesta);
                if (mensaje == "Register succesfull")
                {
                    MessageBox.Show("Usuario Registrado con Éxito", "¡ Información !", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtMail.Text = "";
                    txtUsername.Text = "";
                    txtPassword.Text = "";
                }
                else
                    MessageBox.Show(mensaje + " ", "¡ Error !", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
                
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void RegistroUsuario_Load(object sender, EventArgs e)
        {
            txtMail.Focus();
        }
    }
}
