﻿using ExamenPEv1.Modelo;
using ExamenPEv1.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExamenPEv1
{
    public partial class Login : Form
    {
        RestHelper rh = new RestHelper();
        public Login()
        {
            InitializeComponent();
        }

        private void btnRegistro_Click(object sender, EventArgs e)
        {
            RegistroUsuario window = new RegistroUsuario();
            DialogResult res = window.ShowDialog();
            //var respuesta = await rh.GetAll();
            //txtRespuesta.Text = respuesta;
        }

        private async void btnEntrar_Click(object sender, EventArgs e)
        {
            if (txtUsername.Text == "" || txtPassword.Text == "")
            {
                MessageBox.Show("Completa los campos vacíos", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                var json = await rh.Login(txtUsername.Text, txtPassword.Text);
                string mensaje = rh.MensajeResponse(json);
                if (mensaje == "User authorized")
                {
                    Usuario user = rh.GetUsuario(json);
                    Principal home = new Principal();
                    home.lblUser.Text = user.username;
                    home.lblEmail.Text = user.email;
                    home.lblIdUser.Text = user.idUser.ToString();
                    home.lblUserPhoto.Text = user.userPhoto;
                    home.lblToken.Text = user.token;
                    this.Hide();
                    home.Show();
                }
                else
                    MessageBox.Show(mensaje + " ", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void CerrarPrograma(object sender, FormClosedEventArgs e)
        {
            Application.ExitThread();

        }
        private void CerrarVentana(object sender, FormClosingEventArgs e)
        {

            DialogResult dialogo = MessageBox.Show("¿Desea cerrar el programa?",
               "Cerrar el programa", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogo == DialogResult.No)
            {
                e.Cancel = true;
            }
            else
            {
                e.Cancel = false;

            }
        }

        private void Login_Load(object sender, EventArgs e)
        {
            txtUsername.Focus();
        }
    }
}
